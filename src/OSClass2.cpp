//============================================================================
// Name        : OSClass.cpp
// Author      : chea sovannoty
// Version     :
// Copyright   : free to use
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <iostream>
using namespace std;

float firstMatrix[10][10];
float secondMatrix[10][10];
float multResult[10][10];

class Task {
public:
	int columnFirst;
	int i;
	int columnSecond;

};

void multiplication(int columnFirst, int columnSecond, int i) {

	for (int j = 0; j < columnSecond; ++j) {
		for (int k = 0; k < columnFirst; ++k) {
			multResult[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
			cout << " mult[" << i << "][" << j << "]=" << multResult[i][j]
					<< "\n\n";
		}
	}

}
void *think(void *c_void_ptr) {
	Task *c_int_ptr = (Task *) c_void_ptr;
	multiplication((*c_int_ptr).columnFirst, (*c_int_ptr).columnSecond,
			(*c_int_ptr).i);

}

void enterData(float firstMatrix[][10], float secondMatrix[][10], int rowFirst,
		int columnFirst, int rowSecond, int columnSecond);

void multiplyMatrices(float firstMatrix[][10], float secondMatrix[][10],
		float mult[][10], int rowFirst, int columnFirst, int rowSecond,
		int columnSecond) {
	int i, j, k;

	// Initializing elements of matrix mult to 0.
	for (i = 0; i < rowFirst; ++i) {
		for (j = 0; j < columnSecond; ++j) {
			mult[i][j] = 0;
		}
		// Multiplying matrix firstMatrix and secondMatrix and storing in array mult.
		multiplication(columnFirst, columnSecond, i);


	}

}
void display(float mult[][10], int rowFirst, int columnSecond);

int main() {

	// Calculate the time taken by fun()
	clock_t t;
	t = clock();

	int rowFirst, columnFirst, rowSecond, columnSecond, i, j, k;

	printf("Enter rows and column for first matrix: ");
	scanf("%d %d", &rowFirst, &columnFirst);

	printf("Enter rows and column for second matrix: ");
	scanf("%d %d", &rowSecond, &columnSecond);

	// If colum of first matrix in not equal to row of second matrix, asking user to enter the size of matrix again.
	while (columnFirst != rowSecond) {
		printf("Error! column of first matrix not equal to row of second.\n");
		printf("Enter rows and column for first matrix: ");
		scanf("%d%d", &rowFirst, &columnFirst);
		printf("Enter rows and column for second matrix: ");
		scanf("%d%d", &rowSecond, &columnSecond);
	}

	// Function to take matrices data
	enterData(firstMatrix, secondMatrix, rowFirst, columnFirst, rowSecond,
			columnSecond);

	// Function to multiply two matrices.
	multiplyMatrices(firstMatrix, secondMatrix, multResult, rowFirst,
			columnFirst, rowSecond, columnSecond);

	// Function to display resultant matrix after multiplication.
	display(multResult, rowFirst, columnSecond);
	t = clock() - t;
	double time_taken = ((double) t) / CLOCKS_PER_SEC; // in seconds

	printf("fun() took %f seconds to execute \n", time_taken);

	return EXIT_SUCCESS;
}

void enterData(float firstMatrix[][10], float secondMatrix[][10], int rowFirst,
		int columnFirst, int rowSecond, int columnSecond) {
	int i, j;
	printf("\nEnter elements of matrix 1:\n");
	for (i = 0; i < rowFirst; ++i) {
		for (j = 0; j < columnFirst; ++j) {
			printf("Enter elements a%d%d: ", i + 1, j + 1);
			scanf("%f", &firstMatrix[i][j]);
		}
	}

	printf("\nEnter elements of matrix 2:\n");
	for (i = 0; i < rowSecond; ++i) {
		for (j = 0; j < columnSecond; ++j) {
			printf("Enter elements b%d%d: ", i + 1, j + 1);
			scanf("%f", &secondMatrix[i][j]);
		}
	}
}

void display(float mult[][10], int rowFirst, int columnSecond) {
	int i, j;
	printf("\nOutput Matrix:\n");
	for (i = 0; i < rowFirst; ++i) {
		for (j = 0; j < columnSecond; ++j) {
			printf("%f  ", mult[i][j]);
			if (j == columnSecond - 1)
				printf("\n\n");
		}
	}
}

